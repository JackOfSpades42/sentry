# Sentry Contribution Guide

Here's a quick guide to how **Sentry** has been set up, it covers three main sections:

* Doing QA and Bug Reporting
* Branching, Merge Requests, and Conribution Code

-----

# QA & Bug Reporting

## Doing Quality Assurance

Quality Assurance is a really simple to execute job, but is absolutely invaluable to make sure that Sentry runs as well as it possibly can.  All you really need to do to run QA for us is just go through various flows of the module/api and report any bugs or abnormalities that you find as you go along.  That's it!  

In addition to basic flows, you can also purposely try to use methods or techniques that you think might intentionally break the site, so that we know where vulnerabilites are.  Move fast, break as much as you can, then report what you broke so it can be fixed.

## Reporting Bugs

If you find a bug, the best thing to do is report it on the [issues page of the repository](https://gitlab.com/DogShell_Development/sentry/issues).  Make sure when you log the issue, you apply a label that appropriately designates its severity:
* **Minor**: Small visual or interaction bugs that are apparent, but don't really effect usability in any way (*e.g. Misspelling, spacing oddity*)
* **Major**: Bugs which impact usability of the site in some significant way (*e.g. Responses not formatted correctly, or not returning under some specific conditions*)
* **Critical**: Bugs which render parts of the site completely unusable, or severely threaten the security or structure of the module (*e.g. Users using a security hole caused by the module*)

### Report Structure

All reports are useful, but to make sure there is no ambiguity and that we can easily address the issue, try to stick to the following format when logging a bug (example using markdown format, which you [can read up on here](https://blog.ghost.org/markdown/))


```markdown
**Browser:** (Chrome/Firefox/Edge) (Version)

**User State:** (Logged In/Logged Out/Any)

**Environment:** (Test Server/Local Server/Other)

**Version or Branch:** (v1.2.4/MR#6)

**Description:** (Short description of the issue)

**Steps to Reproduce:**

1. Step One
2. Step Two
3. Step Three

**Screenshots:** (Link to Screenshots/Uploaded Screenshots)

**Add'l Info:** (Anything not covered by the above that's relevant)

```

Feel free to include things like debugger logs too if you're able to generate them with the issue.  More is always better!


### Suggestions and Feature Requests
If while using this module you think of some features or even small quality of life enhancements, please go and make a ticket for them too!  Just use the **Request** labels instead of the bug labels.

-----

# Bug Fixing and Code Contribution

## Commit Format

Commits should be written in present tense :

`"Fix logic error in code"` (not `"Fixing logic error in code"`)

Commits should also reference tickets and issues when possible, by referencing their number in messages:

`"Fix issue with header links (#5)"`


## Branching and Merge Requests

In order to maintain code quality in the project, pushing directly to `dev` or `master` is not allowed.  Instead, when beginning a contribution, create a new branch relevant to the work you'll be doing using `git checkout -b <branch-name>`.  

Branch names should be descriptive--for bug fixes use names like `bug-#` where # is the issue number for the bug.  
For features, use titles like 	`feature-<description>` where description is a quick description of the feature(s) being added to the page.  

*Try to keep branches focused to one issue/feature at a time.*

Multiple people are welcome to work within one branch, just make sure to keep pulls and commits in sync.

When you're ready to make a Merge Request, make sure your branch is up to date with `dev` by running `git pull origin dev`.  If there are any conflicts, make sure to resolve them and commit before opening your MR.

-----

## Merge Requests

When you're satisfied with your work, it's up to date with `dev`, and are ready for it to be merged back into the codebase, go [and open up a merge request within the repository](https://gitlab.com/DogShell_Development/sentry/merge_requests).  

**For now we should merge all branches back into `dev`**. When we finish milestones, we'll merge into master.  

When you get your merge request ready, pop into discord and ask someone to do a code review the request, or assign it to somone in GitLab.  

### Reviewing Requests

Reviewing requests ensures that nothing looks awry before we pull things back into dev. This means someone takes a look at the code to make sure nothing is out of place or not adhering to standrads or best practices.  

If, as a reviewer, you have suggestions, just make a comme t on the merge request or on the specific file line so they can be fixed or addressed.  If everything looks good, just leave a "Looks Good to Me" (LGTM) comment on the MR, and set it's status to 'Approved'

Once a MR has been Approved, recieves an LGTM, and passes all build tests, go ahead and merge it into dev.  

### Closing tickets

If a merge request has been accepted for a bug ticket, make sure it's assigned to someone else to check.  

If you need the site pushed online for people to check, simply ping me to push it--otherwise people are free to pull down a local copy and test that way. (I will be setting up commands to deploy to test shortly, once I configure a test server)

### Versioning Changes

Every time an MR is fixed and merged into `dev`, you should update the version and tag it.

To do this, first make sure to pull the latest from `dev` after merging, and then bump the package version and tag the repo.

You can do this "automagically" by running `npm version <patch/minor/major>`.  It will bump the package version appropriately, and also create a tag

- Use `patch` for bug fixes or simple changes
- Use `minor` for new features (like a new page or workflow)
- We should not have any use for `major` versioning

After running `npm version` you can update and push tags by running `git push origin dev && git push origin dev --tags`

## Code quality

After a change make sure that:
* Old tests are passing
* Linter runs without errors
* You created new tests for the added features/fixed bugs

### Creating new tests

The project uses [tape](https://github.com/substack/tape) as a test suite and [proxyquire](https://github.com/thlorenz/proxyquire) for mocking required modules.

The following is a basic example of a `tape` test with `proxyquire`:
```js
var test = require('tape');

test('test MyModule testFunction', function (assert) {
    assert.plan(2);
    const expectedValue = 'any-value';
    const expectedFilename = 'any-filename';
    const fileContent = 'anything';
    const fsStub = {
      readFileSync: function(filename) {
        assert.equal(filename, expectedFilename, 'Filenames should match');
        return fileContent;
      }
    };
    const MyModule = proxyquire('./mymodule', {
      'fs': fsStub
    });
    let returnedValue = MyModule.testFunction();
    assert.equal(returnedValue, expectedValue);
});
```
Feel free to follow the links for each of those and learn more about them. More assert functions can be found in tape's github page. Also you can take a look at the tests for this project in the `test` folder.

About mocking/stubbing: this technique is used to have a controlled environment to test the desired module, most of the time you want to mock every module that is imported with require in the module.

### Where to add tests
  * If you add a new module in `src` add the corresponding `[module-name].test.js` to the `test` folder
  * If you added a function to a module that already exists add the needed tests at the end of the file
