# Sentry
A library to gather player MMR, stats, and other info

## Installation

Installation instructions need to be updated when the package is added to npm.

## Usage Example

Example needs to be updated when the package is added to npm.

``` js
var SentryModule = require('sentry');
var sentry = new SentryModule.Sentry({
  user: {
    name: 'my-name-to-show',
    username: 'my-username',
    password: 'my-password',
    status: 'Busy',
    guardCode: '1RRRR'
  }
});

sentry.getPlayerMMR(55555555).then(function(MMR) {
  console.log(`Solo MMR: ${MMR.solo}, Party MMR: ${MMR.party}`);
});
```
## Methods

### new Sentry(configuration)

Using the new keyword you create a Sentry object, receives as parameter a configuration object in the following format:
```js
{
  user: {
    username: '<string>',
    password: '<string>',
    name: '<string>',
    status: '<string>',
    guardCode: '<string>'
  }
}
```
* `name` and `status` are optional
* `guardCode` needs to be empty the first time and then filled with the value from steam guard, you need to call the `login()` method to get code
* This will perform a Steam login and launch Dota 2

### waitUntilReady(timeLimit)

Returns a promise that resolves when the module is ready to receive other calls. Used internally.

### getPlayerMMR(playerId)

Gets a player solo and party MMR from the player id. `playerId` is the 8-digit profile ID from dota 2.

Returns a Promise that resolves with an object with the following format:
```js
{
  solo: 9000,
  party: 9000
}
```

### createLobby(options)

Creates a lobby with the specified options, it will also move the client to the spectators list. Returns a promise that resolves if the lobby is created correctly.

The options object needs to have the following format:

```js
{
  name: <string>,
  region: <ServerRegion>,
  mode: <GameMode>,
  seriesType: <SeriesType>,
  cheats: <boolean>,
  fillWithBots: <boolean>,
  allowSpectators: <boolean>,
  password: <string>,
  firstPick: <PickPriority>,
  radiantWins: <number>,
  direWins: <number>,
  allchat: <boolean>,
  delay: <GameDelay>,
	leagueId: <number>
}
```
Values for GameDelay, GameMode, PickPriority, SeriesType and ServerRegion can be found in their respective enums in the module, e.g. `SentryModule.GameMode.CM`.

### updateLobby(options)

Updates the current lobby with the specified options. Returns a promise that resolves if the options were set correctly.

The options object needs to have the following format:

```js
{
  name: <string>,
  region: <ServerRegion>,
  mode: <GameMode>,
  seriesType: <SeriesType>,
  cheats: <boolean>,
  fillWithBots: <boolean>,
  allowSpectators: <boolean>,
  password: <string>,
  firstPick: <PickPriority>,
  radiantWins: <number>,
  direWins: <number>,
  allchat: <boolean>,
  delay: <GameDelay>,
	leagueId: <number>
}
```
Values for GameDelay, GameMode, PickPriority, SeriesType and ServerRegion can be found in their respective enums in the module, e.g. `SentryModule.GameMode.CM`.

### getLobbyInfo()

Gets an object representing a dota lobby. Returns a promise that resolves with the lobby info.

The object will have the following format:

```js
{
  lobbyId: <Long>,
  matchId: <Long>,
  options: {
    name: <string>,
    region: <ServerRegion>,
    mode: <GameMode>,
    seriesType: <SeriesType>,
    cheats: <boolean>,
    fillWithBots: <boolean>,
    allowSpectators: <boolean>,
    password: <string>,
    firstPick: <PickPriority>,
    radiantWins: <number>,
    direWins: <number>,
    allchat: <boolean>,
    delay: <GameDelay>,
    leagueId: <number>
  }
}
```
Values for GameDelay, GameMode, PickPriority, SeriesType and ServerRegion can be found in their respective enums in the module, e.g. `SentryModule.GameMode.CM`.

### leaveLobby()

Leaves the current lobby. Returns a promise that resolves if the lobby was left correctly.

### swapTeamsInLobby()

Changes the team's sides for the current lobby. Returns a promise that resolves if the teams swapped sides correctly.

## Development

### Dependencies

* [Node](https://nodejs.org/)
* [npm](https://www.npmjs.com/)
* [eslint](http://eslint.org/)
* [tape](https://github.com/substack/tape)
* [proxyquire](https://github.com/thlorenz/proxyquire)
* [node-steam](https://github.com/seishun/node-steam)
* [node-dota](https://github.com/Arcana/node-dota2)

### Setup

* Download or clone the repository
* Node and npm need to be installed in your computer
* Run `npm install`

### Testing

`npm test` can be used to run unit tests

### Linter

`npm run lint` can be used to run the linter
