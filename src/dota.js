'use strict';
const dota2 = require('dota2');
const Constants = require('./constants');

/** Class representing a sentry dota 2 object. */
class SentryDota2 {
	/**
	 * Create the sentry dota 2 object
	 * @param {SentrySteam} sentrySteam the sentry steam object from which to open
	 */
	constructor (sentrySteam) {
		this.steamClient = sentrySteam.steamClient;
		this.client = new dota2.Dota2Client(this.steamClient);
	}

	/**
	 * Open the dota 2 client and notices when it is ready
	 * @returns {Promise}
	 */
	launch () {
		return new Promise((resolve, reject) => {
			this.client.launch();
			this.client.on('ready', resolve);
			this.client.on('unready', reject);
		});
	}

	/**
	 * Get a player MMR from the profile card
	 * @param {number} playerId
	 * @returns {Promise} resolves to an object like { solo: <number>, party: <number> }
	 */
	getPlayerMMR (playerId) {
		// Get profile card information
		return new Promise((resolve, reject) => this.client.requestProfileCard(
				playerId,
				(err, data) => {
					if (err) {
						return reject(err);
					}
					// Filter by stat_id (1 for solo, 2 for party)
					// Map to get only the stat property
					// Reduce to an object like { solo: <number>, party: <number> }
					let mmr = data.slots.filter(
						slot => slot.stat && (slot.stat.stat_id === 1 || slot.stat.stat_id === 2))
						.map(slot => slot.stat)
						.reduce((obj, stat) => Object.assign(obj, {
							[stat.stat_id === 1 ? 'solo' : 'party']: stat.stat_score
						}), {});
					return resolve(mmr);
				}
			)
		);
	}

	/**
	 * Create a lobby and join the spectators
	 * @param {object} options
	 * @returns {Promise} resolves if the lobby was created correctly
	 */
	createLobby(options) {
		// Convert options so node-dota understands
		let lobbyOptions = Object.keys(options).reduce((newObject, key) => {
			let newKey = Constants.LobbyOptions[key] || key;
			newObject[newKey] = options[key];
			return newObject;
		}, {});
		return new Promise((resolve, reject) => {
			// Create the lobby
			this.client.createPracticeLobby(options.password, lobbyOptions, err => {
				if(err) {
					return reject(err);
				}
				// Join the spectators
				this.client.joinPracticeLobbyTeam(2, 4, (err, data) => {
					return err ? reject(err) : resolve(data);
				});
			});
		});
	}

	/**
	 * Update a lobby
	 * @param {object} options
	 * @returns {Promise} resolves if the lobby was updated correctly
	 */
	updateLobby(options) {
		let lobbyOptions = Object.keys(options).reduce((newObject, key) => {
			let newKey = Constants.LobbyOptions[key] || key;
			newObject[newKey] = options[key];
			return newObject;
		}, {});
		return new Promise((resolve, reject) => {
			this.client.configPracticeLobby(this.client.Lobby.lobby_id, lobbyOptions, (err, data) => {
				return err ? reject(err) : resolve(data);
			});
		});
	}

	/**
	 * Leave a lobby
	 * @returns {Promise} resolves if the client left correctly
	 */
	leaveLobby() {
		return new Promise((resolve, reject) => {
			this.client.leavePracticeLobby((err, data) => {
				return err ? reject(err) : resolve(data);
			});
		});
	}

	/**
	 * Get lobby information
	 * @returns {Promise} resolves to an object representing the lobby
	 */
	getLobbyInfo() {
		return new Promise((resolve, reject) => {
			if(!this.client.Lobby) {
				reject();
			} else {
				let lobby = this.client.Lobby;
				let availableOptions = Object.keys(Constants.LobbyOptions)
					.reduce((newObject, key) => {
						newObject[Constants.LobbyOptions[key]] = key;
						return newObject;
					}, {});
				let options = Object.keys(lobby).filter(key => Object.keys(availableOptions).indexOf(key) > -1)
					.reduce((newObject, key) => {
						newObject[availableOptions[key]] = lobby[key];
						return newObject;
					}, {});
				resolve({
					lobbyId: lobby.lobby_id,
					matchId: lobby.match_id,
					options
				});
			}
		});
	}

	/*
	 * Swap the teams in a lobby
	 * @returns {Promise} resolves if the teams were swapped correctly
	 */
	swapTeamsInLobby() {
		return new Promise((resolve, reject) => {
			this.client.flipLobbyTeams((err, data) => {
				return err ? reject(err) : resolve(data);
			});
		});
	}
}

module.exports = SentryDota2;
