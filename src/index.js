'use strict';
const SentrySteam = require('./steam');
const SentryDota = require('./dota');
const SentryConstants = require('./constants');

/** Class representing a sentry object.
 *	wrapper for node-steam and node-dota
 */
class Sentry {
	/**
	 * Create the sentry object
	 * @param {Object} config object with the config params
	 */
	constructor(config) {
		this.userConfig = config.user;
		this.steam = new SentrySteam();
		this.isSteamClientReady = false;
		this.isDotaClientReady = false;
		this.error = null;
		this.errorCode = null;
		this.steam.login(
			this.userConfig.username,
			this.userConfig.password,
			this.userConfig.guardCode
		).then(() => {
			this.isSteamClientReady = true;
			this.steam.setProfileInfo(this.userConfig.name, this.userConfig.status);
			this.dota = new SentryDota(this.steam);
			this.dota.launch().then(
				() => this.isDotaClientReady = true,
				errorCode => {
					this.isDotaClientReady = false,
					this.error = 'Dota 2 has failed to start';
					this.errorCode = errorCode;
				}
			);
		}, errorCode => {
			this.isSteamClientReady = false,
			this.error = 'Steam has failed to start';
			this.errorCode = errorCode;
		});
	}

	/**
	 * Wait until the dota 2 client is ready
	 * timeLimit {number} timeLimit maximum time to wait before returning an error
	 * @returns {Promise} resolves if the client is ready, rejects if there is an error
	 */
	waitUntilReady(timeLimit = 30000) {
		return new Promise((resolve, reject) => {
			// if time is up
			if(timeLimit <= 0) {
				let client = !this.isSteamClientReady ? 'Steam' : 'Dota2';
				return reject({
					error: `${client} has taken to much time to start, try again later.`,
					code: -1
				});
			}
			// if steam login or dota 2 launch have failed
			if(this.error) {
				let client = !this.isSteamClientReady ? 'Steam' : 'Dota2';
				return reject({
					error: this.error || `${client} has not started yet, try again later.`,
					code: this.errorCode || -1
				});
			}
			// while steam and dota are not ready
			if(!this.isSteamClientReady || !this.isDotaClientReady) {
				setTimeout(() => this.waitUntilReady(timeLimit - 100).then(resolve, reject), 100);
				return;
			}
			// everything is ready
			resolve();
		});
	}

	/**
	 * Get a player MMR by player id
	 * @param {number} playerId
	 * @returns {Promise} resolves to an object like { solo: <number>, party: <number> }
	 */
	getPlayerMMR(playerId) {
		return this.waitUntilReady().then(() => this.dota.getPlayerMMR(playerId));
	}

	/**
	 * Create a lobby
	 * @param {object} options
	 * @returns {Promise} resolves if the lobby was created correctly
	 */
	createLobby(options) {
		return this.waitUntilReady().then(() => this.dota.createLobby(options)).catch(err => {
			return Promise.reject({
				error: 'Error trying to create the lobby, the client might be in a lobby or some options are not valid.',
				code: err
			});
		});
	}

	/**
	 * Update a lobby
	 * @param {object} options
	 * @returns {Promise} resolves if the lobby was updated correctly
	 */
	updateLobby(options) {
		return this.waitUntilReady().then(() => this.dota.updateLobby(options)).catch(err => {
			return Promise.reject({
				error: 'Error trying to edit the lobby, the client might not be a lobby or some options are not valid.',
				code: err
			});
		});
	}

	/**
	 * Leave a lobby
	 * @returns {Promise} resolves if the client left correctly
	 */
	leaveLobby(options) {
		return this.waitUntilReady().then(() => this.dota.leaveLobby(options))
			.catch(err => {
				return Promise.reject({
					error: 'Error trying to leave the lobby, the client might not be in a lobby',
					code: err
				});
			});
	}

	/**
	 * Get lobby information
	 * @returns {Promise} resolves to an object representing the lobby
	 */
	getLobbyInfo () {
		return this.waitUntilReady().then(() => this.dota.getLobbyInfo())
			.catch(err => {
				return Promise.reject({
					error: 'Error trying to get the lobby, the client might not be in a lobby',
					code: err
				});
			});
	}

	/**
	 * Swap the teams in a lobby
	 * @returns {Promise} resolves if the teams were swapped correctly
	 */
	swapTeamsInLobby() {
		return this.waitUntilReady().then(() => this.dota.swapTeamsInLobby())
			.catch(err => {
				return Promise.reject({
					error: 'Error trying swapping teams, the client might not be in a lobby',
					code: err
				});
			});
	}
}

module.exports = Object.assign({ Sentry }, SentryConstants.Public);
