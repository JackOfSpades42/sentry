'use strict';
const crypto = require('crypto');
const fs = require('fs');
const steam = require('steam');

/** Class representing a sentry steam object. */
class SentrySteam {

	/**
	 * Create the sentry steam object
	 */
	constructor () {
		this.steamClient = new steam.SteamClient();
		this.steamUser = new steam.SteamUser(this.steamClient);
		this.steamFriends = new steam.SteamFriends(this.steamClient);

		// Update sentry file
		this.steamUser.on('updateMachineAuth', (sentry, callback) => {
			let hashedSentry = crypto.createHash('sha1').update(sentry.bytes).digest();
			fs.writeFileSync('sentry', hashedSentry);
			callback({
				sha_file: hashedSentry
			});
		});

		// Update servers file
		this.steamClient.on('servers',
			servers => fs.writeFile('servers', JSON.stringify(servers))
		);
	}

	/**
	 * Login to the steam client
	 * @param {string} username steam username
	 * @param {string} password steam password
	 * @param {string} guardCode authorization code from steam
	 * @returns {Promise}
	 */
	login (username, password, guardCode) {
		// Object sent to the steam client
		let loginDetails = {
			account_name: username,
			password
		};

		// set guard code if it exists
		if (guardCode) {
			loginDetails.auth_code = guardCode;
		}

		// Read sentry file if it exists
		try {
			let sentry = fs.readFileSync('sentry');
			if (sentry.length) {
				loginDetails.sha_sentryfile = sentry;
			}
		} catch (e) {
			// Not an error, not logging for now
		}

		return new Promise((resolve, reject) => {
			this.steamClient.connect();
			// Login, only passing authCode if it exists
			this.steamClient.on('connected', () => this.steamUser.logOn(loginDetails));
			this.steamClient.on('error', reject);
			this.steamClient.on('logOnResponse',
				// Check if the response is what we expect
				response =>
					response.eresult === steam.EResult.OK
					? resolve()
					: reject(response.eresult)
			);
		});
	}

	/**
	 * Change name and status of the current account
	 * @param {string} name
	 * @param {string} status needs to be one of the names from steam.EPersonaState
	 * @returns {Promise}
	 */
	setProfileInfo (name, status) {
		if(name) {
			this.steamFriends.setPersonaName(name);
		}
		if(status && steam.EPersonaState[status]) {
			this.steamFriends.setPersonaState(steam.EPersonaState[status]);
		}
	}
}

module.exports = SentrySteam;
