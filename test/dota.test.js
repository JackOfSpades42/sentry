'use strict';
const proxyquire = require('proxyquire');
const test = require('tape');

test('test Sentry Dota creation', function(assert) {
	assert.plan(3);
	// Create stubs
	const dotaStub = {
		Dota2Client: class {
			constructor(steamClient) {
				assert.equal(steamClient, 'steam-client', 'Sentry Steam client should match in dota constructor');
			}
		}
	};
	// Proxy the subject
	const SentryDota = proxyquire('../src/dota', {
		'dota2': dotaStub
	});
	// Test body
	let subject = new SentryDota({
		steamClient: 'steam-client'
	});
	assert.equal(subject.steamClient, 'steam-client', 'Sentry Steam client should match');
	assert.ok(subject.client instanceof dotaStub.Dota2Client);
});

test('test Sentry Dota launch success', function(assert) {
	assert.plan(2);
	// Create stubs// Create stubs
	const dotaStub = {
		Dota2Client: class {
			launch() {
				assert.pass('Dota2 launch called.');
			}
			on(eventName, callback) {
				if(eventName === 'ready') {
					callback();
				}
			}
		}
	};
	// Proxy the subject
	const SentryDota = proxyquire('../src/dota', {
		'dota2': dotaStub
	});
	// Test body
	let subject = new SentryDota({
		steamClient: 'steam-client'
	});
	subject.launch().then(function() {
		assert.pass('Launch ended successfully');
	});
});

test('test Sentry Dota launch error', function(assert) {
	assert.plan(2);
	// Create stubs// Create stubs
	const dotaStub = {
		Dota2Client: class {
			launch() {
				assert.pass('Dota2 launch called.');
			}
			on(eventName, callback) {
				if(eventName === 'unready') {
					callback();
				}
			}
		}
	};
	// Proxy the subject
	const SentryDota = proxyquire('../src/dota', {
		'dota2': dotaStub
	});
	// Test body
	let subject = new SentryDota({
		steamClient: 'steam-client'
	});
	subject.launch().catch(function() {
		assert.pass('Launch ended with failure');
	});
});

test('test Sentry Dota get player MMR success', function(assert) {
	assert.plan(3);
	// Create stubs// Create stubs
	const expected = {
		solo: 10,
		party: 20
	};
	const response = {
		slots: [{
			stat: {
				stat_id: 1,
				stat_score: 10
			}
		},{
			stat: {
				stat_id: 3,
				stat_score: 30
			}
		},{
			stat: {
				stat_id: 2,
				stat_score: 20
			}
		},{
			stat: null
		}]
	};
	const dotaStub = {
		Dota2Client: class {
			launch() {
				assert.pass('Dota2 launch called.');
			}
			on(eventName, callback) {
				if(eventName === 'ready') {
					callback();
				}
			}
			requestProfileCard(playerId, callback) {
				assert.equal(playerId, 'player-id', 'Player id should match');
				callback(null, response);
			}
		}
	};
	// Proxy the subject
	const SentryDota = proxyquire('../src/dota', {
		'dota2': dotaStub
	});
	// Test body
	let subject = new SentryDota({
		steamClient: 'steam-client'
	});
	subject.launch().then(function() {
		subject.getPlayerMMR('player-id').then(function(MMR){
			assert.deepEqual(MMR, expected, 'MMR object should match the expected');
		});
	});
});

test('test Sentry Dota get player MMR failure', function(assert) {
	assert.plan(3);
	// Create stubs// Create stubs
	const dotaStub = {
		Dota2Client: class {
			launch() {
				assert.pass('Dota2 launch called.');
			}
			on(eventName, callback) {
				if(eventName === 'ready') {
					callback();
				}
			}
			requestProfileCard(playerId, callback) {
				assert.equal(playerId, 'player-id', 'Player id should match');
				callback('error');
			}
		}
	};
	// Proxy the subject
	const SentryDota = proxyquire('../src/dota', {
		'dota2': dotaStub
	});
	// Test body
	let subject = new SentryDota({
		steamClient: 'steam-client'
	});
	subject.launch().then(function() {
		subject.getPlayerMMR('player-id').catch(function(error){
			assert.equal(error, 'error', 'An error is expected');
		});
	});
});

test('test Sentry Dota create lobby', function(assert) {
	assert.plan(5);
	// Create stubs// Create stubs
	const options = {
		option1: 'value1',
		option2: 'value2',
		password: 'pass'
	};
	const expectedOptions = {
		lobbyOption1: 'value1',
		lobbyOption2: 'value2',
		password: 'pass'
	};
	const dotaStub = {
		Dota2Client: class {
			createPracticeLobby(pass, options, callback) {
				assert.deepEqual(options, expectedOptions, 'Options should match');
				assert.equal(pass, 'pass', 'Password should match');
				callback(null);
			}
			joinPracticeLobbyTeam(slot, team, callback) {
				assert.equal(slot, 2, 'Slot should be equal to two');
				assert.equal(team, 4, 'Team should be equal to four');
				callback(null, 'response');
			}
		}
	};
	const constantsStub = {
		LobbyOptions: {
			option1: 'lobbyOption1',
			option2: 'lobbyOption2',
		}
	};
	// Proxy the subject
	const SentryDota = proxyquire('../src/dota', {
		'dota2': dotaStub,
		'./constants': constantsStub
	});
	// Test body
	let subject = new SentryDota({
		steamClient: 'steam-client'
	});
	subject.createLobby(options).then(function() {
		assert.pass('Create lobby ended successfully');
	});
});

test('test Sentry Dota leave lobby', function(assert) {
	assert.plan(2);
	// Create stubs// Create stubs
	const dotaStub = {
		Dota2Client: class {
			leavePracticeLobby(callback) {
				assert.pass('Leave practice lobby was called');
				callback(null);
			}
		}
	};
	// Proxy the subject
	const SentryDota = proxyquire('../src/dota', {
		'dota2': dotaStub
	});
	// Test body
	let subject = new SentryDota({
		steamClient: 'steam-client'
	});
	subject.leaveLobby().then(function() {
		assert.pass('Create lobby ended successfully');
	});
});

test('test Sentry Dota create lobby', function(assert) {
	assert.plan(1);
	// Create stubs
	const expectedOptions = {
		lobbyId: 'lobby-id',
		matchId: 'match-id',
		options: {
			option1: 'value1',
			option2: 'value2'
		}
	};
	const dotaStub = {
		Dota2Client: class {
			constructor() {
				this.Lobby = {
					lobby_id: 'lobby-id',
					match_id: 'match-id',
					lobbyOption1: 'value1',
					lobbyOption2: 'value2',
					lobbyOption3: 'value3'
				};
			}
		}
	};
	const constantsStub = {
		LobbyOptions: {
			option1: 'lobbyOption1',
			option2: 'lobbyOption2',
		}
	};
	// Proxy the subject
	const SentryDota = proxyquire('../src/dota', {
		'dota2': dotaStub,
		'./constants': constantsStub
	});
	// Test body
	let subject = new SentryDota({
		steamClient: 'steam-client'
	});
	subject.getLobbyInfo().then(function(lobbyInfo) {
		assert.deepEqual(lobbyInfo, expectedOptions, 'Lobby info should match');
	});
});

test('test Sentry Dota update lobby', function(assert) {
	assert.plan(3);
	// Create stubs// Create stubs
	const options = {
		option1: 'value1',
		option2: 'value2',
		password: 'pass'
	};
	const expectedOptions = {
		lobbyOption1: 'value1',
		lobbyOption2: 'value2',
		password: 'pass'
	};
	const dotaStub = {
		Dota2Client: class {
			constructor(){
				this.Lobby = { lobby_id: 'lobby-id' };
			}
			configPracticeLobby(lobbyId, options, callback) {
				assert.deepEqual(options, expectedOptions, 'Options should match');
				assert.equal(lobbyId, 'lobby-id', 'Options should match');
				callback(null);
			}
		}
	};
	const constantsStub = {
		LobbyOptions: {
			option1: 'lobbyOption1',
			option2: 'lobbyOption2',
		}
	};
	// Proxy the subject
	const SentryDota = proxyquire('../src/dota', {
		'dota2': dotaStub,
		'./constants': constantsStub
	});
	// Test body
	let subject = new SentryDota({
		steamClient: 'steam-client'
	});
	subject.updateLobby(options).then(function() {
		assert.pass('Update lobby ended successfully');
	});
});

test('test Sentry Dota swap teams in lobby', function(assert) {
	assert.plan(2);
	// Create stubs// Create stubs
	const dotaStub = {
		Dota2Client: class {
			constructor(){
				this.Lobby = { lobby_id: 'lobby-id' };
			}
			flipLobbyTeams(callback) {
				assert.pass('flipLobbyTeams function was called');
				callback(null);
			}
		}
	};
	// Proxy the subject
	const SentryDota = proxyquire('../src/dota', {
		'dota2': dotaStub
	});
	// Test body
	let subject = new SentryDota({
		steamClient: 'steam-client'
	});
	subject.swapTeamsInLobby().then(function() {
		assert.pass('Swap teams in lobby ended successfully');
	});
});
