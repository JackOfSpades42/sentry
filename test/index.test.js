'use strict';
const proxyquire = require('proxyquire');
const test = require('tape');

const testConfig = {
	user: {
		username: 'username1',
		password: 'password1',
		guardCode: 'code1',
		name: 'name1',
		status: 'Busy'
	}
};

test('test Sentry module creation', function(assert) {
	assert.plan(10);
	// Create stubs
	class steamStub {
		constructor() {
			assert.pass('SentrySteam constructor called');
		}
		login(username, password, guardCode) {
			assert.equal(username, 'username1', 'Username should match');
			assert.equal(password, 'password1', 'Password should match');
			assert.equal(guardCode, 'code1', 'Guard code should match');
			return Promise.resolve();
		}
		setProfileInfo(name, status) {
			assert.equal(name, 'name1', 'Name should match');
			assert.equal(status, 'Busy', 'Status should match');
		}
	}
	class dotaStub {
		constructor(st) {
			assert.pass('SentryDota constructor called');
			assert.ok(st instanceof steamStub, 'Should send a SentrySteam object');
		}
		launch() {
			assert.pass('SentryDota launch called');
			return Promise.resolve();
		}
	}
	// Proxy the subject
	const MainModule = proxyquire('../src/index', {
		'./steam': steamStub,
		'./dota': dotaStub
	});
	// Test body
	let subject = new MainModule.Sentry(testConfig);
	assert.deepEqual(subject.userConfig, testConfig.user, 'Configurations should match');
});

test('test Sentry get player MMR', function(assert) {
	assert.plan(3);
	// Create stubs
	class steamStub {
		login() {
			return Promise.resolve();
		}
		setProfileInfo(){}
	}
	const expected = {
		solo: 1234,
		party: 1324
	};
	class dotaStub {
		launch() {
			return Promise.resolve();
		}
		getPlayerMMR(playerId) {
			assert.equal(playerId, 'player-id', 'Player id should match');
			return Promise.resolve(expected);
		}
	}
	// Proxy the subject
	const MainModule = proxyquire('../src/index', {
		'./steam': steamStub,
		'./dota': dotaStub
	});
	// Test body
	let playerId = 'player-id';
	let subject = new MainModule.Sentry(testConfig);
	subject.waitUntilReady = (limit) => {
		assert.notOk(limit, 'waitUntilReady should be called with empty params');
		return Promise.resolve();
	};
	subject.getPlayerMMR(playerId).then(
		response => assert.equal(response, expected,
			'Response should match resolved value from SentryDota')
	);
});

test('test Sentry waitUntilReady both clients ready', function(assert) {
	assert.plan(1);
	// Create stubs
	class steamStub {
		login() {
			return Promise.resolve();
		}
		setProfileInfo(){}
	}
	class dotaStub {
		launch() {
			return Promise.resolve();
		}
	}
	// Proxy the subject
	const MainModule = proxyquire('../src/index', {
		'./steam': steamStub,
		'./dota': dotaStub
	});
	// Test body
	let subject = new MainModule.Sentry(testConfig);
	subject.waitUntilReady().then(function() {
		assert.pass('waitUntilReady resolved successflly');
	});
});

test('test Sentry waitUntilReady steam not ready', function(assert) {
	assert.plan(1);
	// Create stubs
	class steamStub {
		login() {
			return Promise.reject();
		}
	}
	// Proxy the subject
	const MainModule = proxyquire('../src/index', {
		'./steam': steamStub
	});
	// Test body
	let subject = new MainModule.Sentry(testConfig);
	subject.waitUntilReady().catch(function() {
		assert.pass('waitUntilReady failed to resolve');
	});
});

test('test Sentry waitUntilReady dota not ready', function(assert) {
	assert.plan(1);
	// Create stubs
	class steamStub {
		login() {
			return Promise.resolve();
		}
		setProfileInfo(){}
	}
	class dotaStub {
		launch() {
			return Promise.reject();
		}
	}
	// Proxy the subject
	const MainModule = proxyquire('../src/index', {
		'./steam': steamStub,
		'./dota': dotaStub
	});
	// Test body
	let subject = new MainModule.Sentry(testConfig);
	subject.waitUntilReady().catch(function() {
		assert.pass('waitUntilReady failed to resolve');
	});
});


test('test Sentry waitUntilReady time limit', function(assert) {
	assert.plan(1);
	// Create stubs
	class steamStub {
		login() {
			return new Promise((resolve) => {
				setTimeout(resolve, 2000);
			});
		}
		setProfileInfo(){}
	}
	class dotaStub {
		launch() {
			return Promise.reject();
		}
	}
	// Proxy the subject
	const MainModule = proxyquire('../src/index', {
		'./steam': steamStub,
		'./dota': dotaStub
	});
	// Test body
	let subject = new MainModule.Sentry(testConfig);
	subject.waitUntilReady(1000).catch(function() {
		assert.pass('waitUntilReady failed to resolve');
	});
});

test('test Sentry createLobby', function(assert) {
	assert.plan(2);
	// Create stubs
	class steamStub {
		login() {
			return Promise.resolve();
		}
		setProfileInfo(){}
	}
	class dotaStub {
		launch() {
			return Promise.resolve();
		}
		createLobby(options) {
			assert.equal(options, 'custom-options', 'The options object should match');
			return Promise.resolve();
		}
	}
	// Proxy the subject
	const MainModule = proxyquire('../src/index', {
		'./steam': steamStub,
		'./dota': dotaStub
	});
	// Test body
	let subject = new MainModule.Sentry(testConfig);
	subject.createLobby('custom-options').then(() => assert.pass('Create lobby worked as expected'));
});

test('test Sentry leaveLobby', function(assert) {
	assert.plan(2);
	// Create stubs
	class steamStub {
		login() {
			return Promise.resolve();
		}
		setProfileInfo(){}
	}
	class dotaStub {
		launch() {
			return Promise.resolve();
		}
	}
	dotaStub.prototype.leaveLobby = function() {
		assert.pass('leaveLobby was called');
		return Promise.resolve();
	};

	// Proxy the subject
	const MainModule = proxyquire('../src/index', {
		'./steam': steamStub,
		'./dota': dotaStub
	});
	// Test body
	let subject = new MainModule.Sentry(testConfig);
	subject.leaveLobby('custom-options').then(() => assert.pass('Leave lobby worked as expected'));
});

test('test Sentry updateLobby', function(assert) {
	assert.plan(2);
	// Create stubs
	class steamStub {
		login() {
			return Promise.resolve();
		}
		setProfileInfo(){}
	}
	class dotaStub {
		launch() {
			return Promise.resolve();
		}
		updateLobby(options) {
			assert.equal(options, 'custom-options', 'The options object should match');
			return Promise.resolve();
		}
	}
	// Proxy the subject
	const MainModule = proxyquire('../src/index', {
		'./steam': steamStub,
		'./dota': dotaStub
	});
	// Test body
	let subject = new MainModule.Sentry(testConfig);
	subject.updateLobby('custom-options').then(() => assert.pass('Update lobby worked as expected'));
});

test('test Sentry get Lobby Info', function(assert) {
	assert.plan(2);
	// Create stubs
	class steamStub {
		login() {
			return Promise.resolve();
		}
		setProfileInfo(){}
	}
	class dotaStub {
		launch() {
			return Promise.resolve();
		}
	}
	dotaStub.prototype.getLobbyInfo = function() {
		assert.pass('getLobbyInfo was called');
		return Promise.resolve();
	};
	// Proxy the subject
	const MainModule = proxyquire('../src/index', {
		'./steam': steamStub,
		'./dota': dotaStub
	});
	// Test body
	let subject = new MainModule.Sentry(testConfig);
	subject.getLobbyInfo().then(() => assert.pass('Get lobby info worked as expected'));
});

test('test Sentry swap teams in lobby', function(assert) {
	assert.plan(2);
	// Create stubs
	class steamStub {
		login() {
			return Promise.resolve();
		}
		setProfileInfo(){}
	}
	class dotaStub {
		launch() {
			return Promise.resolve();
		}
		swapTeamsInLobby() {
			assert.pass('swapTeamsInLobby function was called');
			return Promise.resolve();
		}
	}
	// Proxy the subject
	const MainModule = proxyquire('../src/index', {
		'./steam': steamStub,
		'./dota': dotaStub
	});
	// Test body
	let subject = new MainModule.Sentry(testConfig);
	subject.swapTeamsInLobby().then(() => assert.pass('Swap teams in lobby worked as expected'));
});
