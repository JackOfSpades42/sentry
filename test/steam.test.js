'use strict';
const proxyquire = require('proxyquire');
const test = require('tape');

test('test Sentry Steam creation', function(assert) {
	assert.plan(5);
	// Create stubs
	const steamStub = {
		SteamClient: class {
			constructor() {
				assert.pass('Steam Client constructor called');
			}
			on(event) {
				assert.equal(event, 'servers', 'Client event should match');
			}
		},
		SteamUser: class {
			constructor() {
				assert.pass('Steam User constructor called');
			}
			on(event) {
				assert.equal(event, 'updateMachineAuth', 'User event should match');
			}
		},
		SteamFriends: class {
			constructor() {
				assert.pass('Steam Friends constructor called');
			}
		}
	};
	// Proxy the subject
	const SentrySteam = proxyquire('../src/steam', {
		'steam': steamStub
	});
	// Test body
	new SentrySteam();
});

test('test Sentry Steam login success', function(assert) {
	assert.plan(3);
	const expectedDetails = {
		account_name: 'username1',
		password: 'password1',
		auth_code: 'code1'
	};
	const logOnResponse = {
		eresult: 1
	};
	// Create stubs
	const fsStub = {
		readFileSync: () => {}
	};
	const steamStub = {
		EResult: {
			OK: 1
		},
		SteamClient: class {
			on(event, callback) {
				if(event === 'connected') {
					callback();
				}
				if(event === 'logOnResponse') {
					callback(logOnResponse);
				}
			}
			connect() {
				assert.pass('Steam client connect called');
			}
		},
		SteamUser: class {
			on() {}
			logOn(details) {
				assert.deepEqual(details, expectedDetails, 'Account details should match');
			}
		},
		SteamFriends: class {}
	};
	// Proxy the subject
	const SentrySteam = proxyquire('../src/steam', {
		'steam': steamStub,
		'fs': fsStub
	});
	// Test body
	let subject = new SentrySteam();
	subject.login('username1', 'password1', 'code1').then(function() {
		assert.pass('Should return success');
	});
});

test('test Sentry Steam login error', function(assert) {
	assert.plan(2);
	// Create stubs
	const steamStub = {
		SteamClient: class {
			on(event, callback) {
				if(event === 'error') {
					callback();
				}
			}
			connect() {
				assert.pass('Steam client connect called');
			}
		},
		SteamUser: class {
			on() {}
		},
		SteamFriends: class {}
	};
	// Proxy the subject
	const SentrySteam = proxyquire('../src/steam', {
		'steam': steamStub
	});
	// Test body
	let subject = new SentrySteam();
	subject.login('username1', 'password1').catch(function() {
		assert.pass('Should return error');
	});
});

test('test Sentry Steam login bad response', function(assert) {
	assert.plan(3);
	const expectedDetails = {
		account_name: 'username1',
		password: 'password1'
	};
	const logOnResponse = {
		eresult: 1
	};
	// Create stubs
	const fsStub = {
		readFileSync: () => {}
	};
	const steamStub = {
		EResult: {
			OK: 2
		},
		SteamClient: class {
			on(event, callback) {
				if(event === 'connected') {
					callback();
				}
				if(event === 'logOnResponse') {
					callback(logOnResponse);
				}
			}
			connect() {
				assert.pass('Steam client connect called');
			}
		},
		SteamUser: class {
			on() {}
			logOn(details) {
				assert.deepEqual(details, expectedDetails, 'Account details should match');
			}
		},
		SteamFriends: class {}
	};
	// Proxy the subject
	const SentrySteam = proxyquire('../src/steam', {
		'steam': steamStub,
		'fs': fsStub
	});
	// Test body
	let subject = new SentrySteam();
	subject.login('username1', 'password1').catch(function() {
		assert.pass('Should return error');
	});
});

test('test Sentry Steam set profile info', function(assert) {
	assert.plan(2);
	// Create stubs
	const steamStub = {
		EPersonaState: {
			Busy: 'busy-status'
		},
		SteamClient: class {
			on() {}
		},
		SteamUser: class {
			on() {}
		},
		SteamFriends: class {
			setPersonaName(name) {
				assert.equal(name, 'name1', 'Name should match');
			}
			setPersonaState(status) {
				assert.equal(status, 'busy-status', 'Status should match');
			}
		}
	};
	// Proxy the subject
	const SentrySteam = proxyquire('../src/steam', {
		'steam': steamStub
	});
	// Test body
	let subject = new SentrySteam();
	subject.setProfileInfo('name1', 'Busy');
});

test('test Sentry Steam set profile info no name', function(assert) {
	assert.plan(1);
	// Create stubs
	const steamStub = {
		EPersonaState: {
			Busy: 'busy-status'
		},
		SteamClient: class {
			on() {}
		},
		SteamUser: class {
			on() {}
		},
		SteamFriends: class {
			setPersonaName() {
				assert.fail('setPersonaName should not be called');
			}
			setPersonaState(status) {
				assert.equal(status, 'busy-status', 'Status should match');
			}
		}
	};
	// Proxy the subject
	const SentrySteam = proxyquire('../src/steam', {
		'steam': steamStub
	});
	// Test body
	let subject = new SentrySteam();
	subject.setProfileInfo(null, 'Busy');
});

test('test Sentry Steam set profile info no status', function(assert) {
	assert.plan(1);
	// Create stubs
	const steamStub = {
		EPersonaState: {
			Busy: 'busy-status'
		},
		SteamClient: class {
			on() {}
		},
		SteamUser: class {
			on() {}
		},
		SteamFriends: class {
			setPersonaName(name) {
				assert.equal(name, 'name1', 'Name should match');
			}
			setPersonaState() {
				assert.fail('setPersonaName should not be called');
			}
		}
	};
	// Proxy the subject
	const SentrySteam = proxyquire('../src/steam', {
		'steam': steamStub
	});
	// Test body
	let subject = new SentrySteam();
	subject.setProfileInfo('name1', null);
});

test('test Sentry Steam set profile info wrong status', function(assert) {
	assert.plan(1);
	// Create stubs
	const steamStub = {
		EPersonaState: {},
		SteamClient: class {
			on() {}
		},
		SteamUser: class {
			on() {}
		},
		SteamFriends: class {
			setPersonaName(name) {
				assert.equal(name, 'name1', 'Name should match');
			}
			setPersonaState() {
				assert.fail('setPersonaName should not be called');
			}
		}
	};
	// Proxy the subject
	const SentrySteam = proxyquire('../src/steam', {
		'steam': steamStub
	});
	// Test body
	let subject = new SentrySteam();
	subject.setProfileInfo('name1', null);
});
